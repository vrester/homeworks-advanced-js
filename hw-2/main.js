const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
]

const validateObjKeys = (obj, requiredKeys) => {
    for (const key of requiredKeys) {
        if (!(key in obj)) {
            throw new Error(`Об'єкт ${JSON.stringify(obj)} не має ключа: ${key}`)
        }
    }
}

const createListItems = (obj) => {
    const listItems = Object.entries(obj).map(([key, value]) => `<li>${key}: ${value}</li>`)
    return `<ul>${listItems.join('')}</ul>`
}

// Print the list of books to the root element
const printList = (array, requiredKeys) => {
    const root = document.querySelector('#root')
    for (const obj of array) {
        try {
            validateObjKeys(obj, requiredKeys)
            const list = createListItems(obj)
            root.insertAdjacentHTML('beforeend', list)
        } catch (error) {
            console.log(error.message)
        }
    }
}
printList(books, ['name', 'author', 'price'])


