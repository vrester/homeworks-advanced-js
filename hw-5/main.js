const usersUrl = 'https://ajax.test-danit.com/api/json/users'
const postsUrl = 'https://ajax.test-danit.com/api/json/posts'

const getData = () => {
    return Promise.all([fetch(usersUrl), fetch(postsUrl)])
        .then(([usersResponse, postsResponse]) => {
            return Promise.all([usersResponse.json(), postsResponse.json()])
        })
        .catch(err => console.log(err.text))
}

getData()
    .then(([users, posts]) => initPosts(users, posts))
    .catch(err => console.log(err.text))

const initPosts = (users, posts) => {
    posts.forEach(post => renderPost(post, users))
}

const renderPost = (post, users) => {
    const user = users.find(user => user.id === post.userId)
    const {title, body, id} = post
    const {name, email} = user
    const card = new Card(title, body, name, email, id)
    document.body.insertAdjacentElement('beforebegin', card.getElement());
}

class Card {
    constructor(title, text, name, email, postId) {
        this._title = title
        this._text = text
        this._name = name
        this._email = email
        this._postId = postId
        this._card = this.renderCard();
        this._card.querySelector('.card__button').addEventListener('click', () => this.removeCard())
    }

    renderCard() {
        const element = document.createElement('div')
        element.classList.add('card')
        element.dataset.id = this._postId

        element.innerHTML = `
            <div class="card__user-info">
                <p class="card__user-name">${this._name}</p>
                <p class="card__user-email">${this._email}</p>
                <button class="card__button button"></button>
            </div>
            <div class="card__title">${this._title}</div>
            <div class="card__description">${this._text}</div>
        `
        return element
    }

    removeCard() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this._postId}`, {
            method: 'DELETE',
        })
            .then(response => {
                if (response.ok) {
                    this._card.remove()
                }
            })
            .catch(err => console.log(err.text))
    }

    getElement() {
        return this._card;
    }
}


