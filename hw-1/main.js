class Employee {
    constructor(name = 'Anhelina Kitkat', age = 83, salary = 0) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name;
    }

    get age() {
        return this._age;
    }

    set age(age) {
        this._age = age;
    }

    get salary() {
        return this._salary;
    }

    set salary(salary) {
        this._salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang = []) {
        super(name, age, salary);
        this._lang = lang;
    }

    get lang() {
        return this._lang;
    }

    set lang(languages) {
        this._lang = languages;
    }

    get salary() {
        return super.salary * 3;
    }
    set salary(salary) {
        super.salary = salary;
    }
}

const programmer1 = new Programmer("John", 25, 1000, ['JavaScript', 'C#']);
console.log(programmer1)
const programmer2 = new Programmer("Mary", 30, 5000, 'Java');
console.log(programmer2)