const filmsUrl = 'https://ajax.test-danit.com/api/swapi/films'
const getFilmsData = (url) => {
    fetch(url)
        .then(response => response.json())
        .then(films => renderFilms(films))
        .catch(err => console.log(err.text))
}
const renderFilms = (films) => {
    films.sort((a, b) => a.episodeId - b.episodeId)
    films.forEach(film => {
        const ul = document.createElement('ul')
        const {episodeId, name, openingCrawl} = film
        ul.dataset.id = film.id
        ul.innerHTML = `
        <li>Episode: ${episodeId}</li>
        <li>Name: ${name}</li>
        <li>Description: ${openingCrawl}</li>
        `
        document.body.append(ul)
        getCharactersData(film)
    })
}
const getCharactersData = (film) => {
    const {episodeId, characters} = film
    const charactersPromise = characters.map(url => fetch(url).then(response => response.json()))
    Promise.all(charactersPromise)
        .then(characters => renderCharacters(characters, episodeId))
        .catch(err => console.log(err.text))
}

const renderCharacters = (characters, filmId) => {
    const film = document.querySelector(`[data-id='${filmId}']`)
    const ul = document.createElement('ul')
    characters.forEach(character => {
        const {name} = character
        const li = `<li>Character: ${name}</li>`
        ul.insertAdjacentHTML('beforeend', li)
    })
    film.append(ul)
}
getFilmsData(filmsUrl)