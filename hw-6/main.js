const button = document.querySelector('.button')
const content = document.querySelector('.content')
const userIp = 'https://api.ipify.org/?format=json'
const getIpDetailsUrl = `http://ip-api.com/json/`
const requiredFields = 'status,message,continent,country,region,regionName,city,timezone'

button.addEventListener('click', () => initSearch(userIp))

const getData = async (url) => {
    const response = await fetch(url)
    if (!response.ok) {
        throw new Error(`Network error: ${response.status}`)
    }
    return await response.json()
}

const initSearch = async (ip) => {
    const card = document.querySelector('.card')
    if (card) card.remove()
    try {
        const data = await getData(ip)
        const {ip: ipAddress} = data
        const ipDetails = await getData(`${getIpDetailsUrl}${ipAddress}?fields=${requiredFields}`)
        console.log(ipDetails)
        const {timezone, country, region, city, regionName} = ipDetails
        const userInfo = new User(timezone, country, region, city, regionName)
        const renderedUserInfo = userInfo.renderLocationInfoCard()
        content.insertAdjacentHTML('beforeend', renderedUserInfo)
    } catch (err) {
        console.log(err.text)
    }
}

class User {
    constructor(continent, country, region, city, regionName) {
        this._continent = continent
        this._country = country
        this._region = region
        this._city = city
        this._regionName = regionName
    }

    renderLocationInfoCard() {
        return `
        <div class="card">
            <div class="card-header">
                <h2>Location Information</h2>
             </div>
            <div class="card-body">
                <div class="location-item">
                    <span class="location-label">Continent:</span>
                    <span class="location-value">${this._continent}</span>
                </div>
                <div class="location-item">
                    <span class="location-label">Country:</span>
                    <span class="location-value">${this._country}</span>
                </div>
                <div class="location-item">
                    <span class="location-label">Region:</span>
                    <span class="location-value">${this._region}</span>
                </div>
             <div class="location-item">
                    <span class="location-label">City:</span>
                    <span class="location-value">${this._city}</span>
                </div>
                <div class="location-item">
                    <span class="location-label">Region Name:</span>
                    <span class="location-value">${this._regionName}</span>
                </div>
            </div>
        </div>
        `
    }
}